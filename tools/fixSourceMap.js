#!/usr/bin/env node
// https://github.com/parcel-bundler/parcel/issues/3913
/* eslint-disable no-console */

const fs = require('fs');

const filePath = 'dist/dagsimulator.js';

fs.readFile(filePath, 'utf8', (readErr, contents) => {
  if (readErr) {
    console.log(readErr);
    return;
  }

  const replaced = contents.replace('/dagsimulator.js.map', 'dagsimulator.js.map');

  fs.writeFile(filePath, replaced, 'utf8', (writeErr) => {
    if (writeErr) {
      console.log(writeErr);
    }
  });
});
