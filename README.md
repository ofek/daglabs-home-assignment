# DAGlabs Home Assignment

## Installation

Requires Node.js v14.

To install:

```sh
$ nvm use 14
$ yarn install
$ yarn build
```

## Usage

```sh
$ dist/dagsimulator.js --help
```

### Graph visualizer

There is a graph visualizer accessible by default on `http://localhost:3000`. Note that its implementation is extremely naive and it may clog the browser even with a relatively low miner count.

## Docker

If, for some reason, you don't have access to Node.js v14, you can use Docker.

```sh
# Build the image
$ docker build . -t dagsimulator
# Run like it's a binary
$ docker run -p 3000:3000 -t dagsimulator --help
```

_Note:_ This build uses the full-sized Node.js image. Using Alpine requires more research and testing.

## Development

Project is written in TypeScript, uses AirBnB linting rules and Prettier for formatting. Please install Prettier for your editor or my imaginary CI will fail your build.

```sh
$ yarn dev
```

The above command will, beyond building, will:

1. Lint
2. Type check
3. Watch for file changes
4. Run the output
5. Uses sourcemaps for errors and debugging

Please free to read through the scripts in `package.json` for more useful tooling.

Debug messages are built on top of the `debug` module. **They are very noisy**. To see them, set `DEBUG` environment variable to comma separated list of namespaces. Available ones are:

- `dag:block*`
- `dag:dag*`
- `dag:miner*`
- `dag:miner_simulator`
- `dag:the_internet`
- `dag:surfer*`
- `dag:*` -- for everything

### Git workflow

1. Branch out of master
2. Open MR
3. Work, rebase, repeat
4. Final rebase
5. Merge MR

All MRs create merge commits for useful feature bubbles (see `git log --graph`). Rebase to avoid bubble hell.

## Description & Design

Reading the commit messages is a great resource to understand how things were developed.

As for a more general overview, there are 4 main moving pieces:

### 1. TheInternet™®

Used to simulate message passing between the different actors in the simulation, mainly for the delay and async nature of it.

### 2. Surfer

A surfer is a mixin (see below), that enables interaction with TheInternet™®.

### 3. Miner

A miner is responsible for the actual creation of new blocks and their broadcast.

### 4. MineSimulator && index.ts

Orchestrate the simulator execution

## Mixins

This codebase follows a composition over inheritance approach. TypeScript's support of Mixins is somewhat lacking, however, so some boilerplate code may seem a bit weird, but it's small and unobtrusive.

## Directory Structure

```sh
.
|-- src
|   |-- classes # Put "end-user" classes here
|   |-- mixins # For class augmentation.
|   |-- types # TypeScript typings
|   `-- utils # For shared functionality
`-- tools # For build, ops, deploy, etc.
```
