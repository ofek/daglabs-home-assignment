FROM node:14

RUN mkdir -p /usr/src/app && chown node:node /usr/src/app
WORKDIR /usr/src/app
USER node

COPY --chown=node:node package.json yarn.lock ./
RUN yarn install

COPY --chown=node:node . ./
RUN yarn build

ENTRYPOINT [ "node", "dist/dagsimulator.js" ]
