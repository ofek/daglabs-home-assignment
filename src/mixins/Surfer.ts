import Debug, { Debugger } from 'debug';

import DummyClass from '~/utils/dummyClass.ts';
import TheInternet, { IP, InternetReqRes } from '~/classes/TheInternet';
import { isCool, getCheckState, CheckProgress } from '~/utils/superSecureSecurity';
import { genCoolMessage, genUnknownMessage } from '~/utils/commonMessages';

export type MessageHandler = (message: InternetReqRes) => InternetReqRes;

export type SurferOptions = {
  internet: TheInternet;
};

function Surfer<TBase extends Constructor>(Base: TBase) {
  return class extends Base {
    internet: TheInternet;

    ip: IP = '';

    surferPeers: Set<IP> = new Set();

    messageHandlers: Map<string, MessageHandler> | undefined;

    private surferDebug: Debugger;

    constructor(...args: any[]) {
      super(args);
      const { internet } = args[0];

      this.internet = internet;
      this.ip = this.internet.connect(this);
      this.surferDebug = Debug(`dag:surfer:${this.ip}`);

      this.messageHandlers = this.messageHandlers || new Map();

      this.messageHandlers.set('COOL', (message: InternetReqRes) =>
        this.handleSecurity(message)
      );
    }

    onMessage(message: InternetReqRes): InternetReqRes {
      this.surferDebug('incoming message %O', message);
      const handler = this.messageHandlers!.get(message.type);

      if (handler) {
        this.surferDebug('handling message %O', message);
        return handler(message);
      }

      this.surferDebug('message type %s is unknown', message && message.type);
      return genUnknownMessage(this.ip, message.from);
    }

    addVerifiedSurferPeer(peerIp: IP) {
      this.surferPeers.add(peerIp);
      this.surferDebug('surfer %s added successfully', peerIp);
    }

    async verifySurferPeer(peerIp: IP): Promise<boolean> {
      let response = await this.internet.sendMessage(genCoolMessage(this.ip, peerIp, ''));

      let currState = getCheckState(response.content);
      while (currState === CheckProgress.Ongoing) {
        // loop iterations are dependent
        // eslint-disable-next-line no-await-in-loop
        response = await this.internet.sendMessage(
          genCoolMessage(this.ip, peerIp, response.content)
        );
        currState = getCheckState(response.content);
      }

      if (getCheckState(response.content) === CheckProgress.Passed) {
        return true;
      }

      return false;
    }

    async addSurferPeer(peerIp: IP): Promise<boolean> {
      if (this.hasPeer(peerIp)) {
        return false;
      }

      this.surferDebug('adding surfer %s', peerIp);

      if (await this.verifySurferPeer(peerIp)) {
        this.addVerifiedSurferPeer(peerIp);
        return true;
      }

      return false;
    }

    async addSurferPeers(peerIps: IP[]): Promise<boolean[]> {
      return Promise.all(peerIps.map((ip) => this.addSurferPeer(ip)));
    }

    hasPeer(peerIp: IP) {
      return this.surferPeers.has(peerIp);
    }

    messageAllPeers(type: string, content: string): Promise<InternetReqRes>[] {
      this.surferDebug('messaging all peers');

      return Array.from(this.surferPeers).map((peerIp) =>
        this.internet.sendMessage({
          type,
          content,
          from: this.ip,
          to: peerIp,
        })
      );
    }

    handleSecurity(message: InternetReqRes) {
      const responseContent = isCool(message.content);

      if (responseContent === 'Cool cool cool') {
        this.addVerifiedSurferPeer(message.from);
      }

      return genCoolMessage(this.ip, message.from, responseContent);
    }
  };
}

// A hack to avoid writing an equivalent interface
const DummySurfer = Surfer(DummyClass);
type Surfer = typeof DummySurfer.prototype;

export default Surfer;
