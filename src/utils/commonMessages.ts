import { IP, InternetReqRes } from '~/classes/TheInternet';

export function genCoolMessage(from: IP, to: IP, content: any): InternetReqRes {
  return {
    type: 'COOL',
    content,
    from,
    to,
  };
}

export function genUnknownMessage(from: IP, to: IP) {
  return {
    type: 'UNKNOWN',
    content: 'Whatchu talkin bout Willis?',
    from,
    to,
  };
}

export function genErrorMessage(from: IP, to: IP, content?: string) {
  return {
    type: 'ERROR',
    content: content || '',
    from,
    to,
  };
}
