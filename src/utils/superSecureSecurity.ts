const SECRET_RESPONSES: { [challenge: string]: string | undefined } = {
  'Are you cool?': 'Yeah, are you cool?',
  'Yeah, are you cool?': 'Yeah',
  Yeah: 'Cool',
  Cool: 'Cool cool cool',
  'Not cool': 'Not cool',
};

export enum CheckProgress {
  Passed,
  Failed,
  Ongoing,
}

export function isCool(challenge: string): string {
  if (!challenge) {
    return 'Are you cool?';
  }

  return SECRET_RESPONSES[challenge] || 'Not cool';
}

export function getCheckState(currChallenge: string): CheckProgress {
  switch (currChallenge) {
    case 'Cool cool cool':
      return CheckProgress.Passed;
    case 'Not cool':
      return CheckProgress.Failed;
    default:
      return CheckProgress.Ongoing;
  }
}
