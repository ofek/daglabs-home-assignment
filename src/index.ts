import consola from 'consola';
import yargs from 'yargs';

import MineSimulator from './classes/MineSimulator';
import lift from './visualizer/server';

const Spinnies = require('spinnies');

const spinnies = new Spinnies();

let lastSent = new Date();

async function start(simulator: MineSimulator, io?: SocketIO.Server) {
  let currBlockIndex = 1;

  spinnies.add('matchMaking', { text: `Waiting for peer discovery...` });

  const simStream = await simulator.run();

  spinnies.remove('matchMaking');
  consola.success(`Peer discovery complete`);

  simStream.on('data', (broadcastStream) => {
    const thisBlockIndex = currBlockIndex;
    currBlockIndex += 1;

    consola.success(`Block #${thisBlockIndex} added locally`);
    const broadcastText = `Broadcasting block #${thisBlockIndex}`;
    spinnies.add(`${thisBlockIndex}`, { text: broadcastText });

    broadcastStream.on('data', (peerCount: number) => {
      spinnies.update(`${thisBlockIndex}`, {
        text: `${broadcastText} to ${peerCount} peers`,
      });
    });

    broadcastStream.on('end', () => {
      spinnies.succeed(`${thisBlockIndex}`, {
        text: `All messages sent for block #${thisBlockIndex}`,
      });

      const now = new Date();
      if (+now - +lastSent > 3000) {
        io?.emit(
          'dags',
          simulator.miners.map((m) => m.dag)
        );
        lastSent = now;
      }
    });
  });
}

// eslint-disable-next-line @typescript-eslint/no-unused-expressions
const {
  'block-count': blockCount,
  'miner-count': minerCount,
  'creation-rate': creationRate,
  'propagation-delay': propagationDelay,
  'disable-visual': disableVisual,
  port,
} = yargs
  .usage('Usage: $0 [OPTION]...')
  .option('block-count', {
    alias: 'b',
    type: 'number',
    description: 'Blocks number to create',
    default: 20,
  })
  .option('miner-count', {
    alias: 'm',
    type: 'number',
    description: 'Miners number to create',
    default: 7,
  })
  .option('creation-rate', {
    alias: 'c',
    type: 'number',
    description: 'Blocks creation rate (milliseconds)',
    default: 1000,
  })
  .option('propagation-delay', {
    alias: 'p',
    type: 'number',
    description: 'Communication delay (milliseconds)',
    default: 2500,
  })
  .option('port', {
    type: 'number',
    description: 'Visualizations server port',
    default: 3000,
  })
  .option('disable-visual', {
    type: 'boolean',
    description: 'Disable visualizations',
    default: false,
  }).argv;

const options = {
  blockCount,
  minerCount,
  creationRate,
  propagationDelay,
};

consola.info('Simulating with the options: ', options);

const io = disableVisual ? undefined : lift(port);
const simulator = new MineSimulator(options);

io?.on('connect', () => {
  io.emit(
    'dags',
    simulator.miners.map((m) => m.dag)
  );
  lastSent = new Date();
});

start(simulator, io);
