import http from 'http';
import socketio from 'socket.io';

import graphPage from './index.html';

export default function lift(port: number) {
  const server = http.createServer((req, res) => {
    res.setHeader('Content-Type', 'text/html');
    res.writeHead(200);
    res.end(graphPage);
  });

  const io = socketio(server);

  server.listen(port);

  return io;
}
