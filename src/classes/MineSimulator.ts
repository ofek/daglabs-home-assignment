import Debug from 'debug';
import * as stream from 'stream';

import Miner from '~/classes/Miner';
import getRandomInt from '~/utils/getRandomInt';
import TheInternet from '~/classes/TheInternet';

const { Readable } = stream;

type MineSimulatorOptions = {
  blockCount: number;
  minerCount: number;
  creationRate: number;
  propagationDelay: number;
};

// Independent of instance
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const debug = Debug('dag:mine_simulator');

export default class MineSimulator {
  miners: Miner[];

  blockCount: number;

  creationRate: number;

  propagationDelay: number;

  internet: TheInternet;

  constructor({
    blockCount,
    minerCount,
    creationRate,
    propagationDelay,
  }: MineSimulatorOptions) {
    this.blockCount = blockCount;
    this.creationRate = creationRate;
    this.propagationDelay = propagationDelay;

    this.internet = new TheInternet({ propagationDelay });

    this.miners = new Array(minerCount)
      .fill(undefined)
      .map(() => new Miner({ internet: this.internet }));
  }

  async run() {
    await this.matchMake();
    debug('starting run...');
    return Readable.from(this.randomMineGenerator());
  }

  async *randomMineGenerator() {
    let countdown = this.blockCount;

    while (countdown) {
      debug('%d blocks left to mine', countdown);
      // emulated sleep is part of the exercise requirements
      // eslint-disable-next-line no-await-in-loop
      await new Promise((resolve) => setTimeout(resolve, this.creationRate));
      yield this.randomMinerMine();
      countdown -= 1;
    }
  }

  randomMinerMine() {
    const minerIndex = getRandomInt(0, this.miners.length);
    const miner = this.miners[minerIndex];

    debug('mining with miner %s', miner.id);

    return miner.randomParentsMine();
  }

  matchMake() {
    debug('match making...');
    return Promise.all(
      this.miners.map(async (miner) =>
        miner.addSurferPeers(
          this.miners.filter((m) => m.ip !== miner.ip).map((m) => m.ip)
        )
      )
    );
  }
}
