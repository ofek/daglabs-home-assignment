import { v4 as uuid } from 'uuid';
import Debug, { Debugger } from 'debug';

import Block, { SerializedBlock } from '~/classes/Block';

export type SerializedDag = {
  id: string;
  tips: SerializedBlock[];
};

type DagOptions = {
  id?: string;
  tips?: Block[];
};

export default class DAG {
  id: string;

  tips: Block[];

  private debug: Debugger;

  constructor({ id, tips }: DagOptions) {
    this.id = id || uuid();
    this.tips = tips || [
      new Block({
        id: 'GENESIS',
      }),
    ];

    this.debug = Debug(`dag:dag:${this.id}`);
  }

  addBlock(block: Block) {
    this.debug('adding block %O', block);
    const filteredTips = this.tips.filter(
      (tip) => !block.parents.some((p) => p.id === tip.id)
    );

    filteredTips.push(block);
    this.tips = filteredTips;
    this.debug('block %s added, tips updated', block.id);
    return true;
  }

  serialize(): SerializedDag {
    return {
      id: this.id,
      tips: this.tips.map((t) => t.serialize()),
    };
  }

  static deserialize(serializedDag: SerializedDag, isNewId?: boolean) {
    try {
      const { id, tips: parsedTips } = serializedDag;
      const tips: Block[] = parsedTips.map((t: SerializedBlock) => Block.deserialize(t));

      return new DAG({ tips, id: isNewId ? undefined : id });
    } catch {
      throw new Error('Bad input');
    }
  }
}
