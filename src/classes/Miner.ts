import { v4 as uuid } from 'uuid';
import Debug, { Debugger } from 'debug';
import * as stream from 'stream';

import DAG from '~/classes/DAG';
import Block from '~/classes/Block';
import Surfer, { SurferOptions, MessageHandler } from '~/mixins/Surfer';

import getRandomInt from '~/utils/getRandomInt';
import { genErrorMessage } from '~/utils/commonMessages';
import { InternetReqRes } from '~/classes/TheInternet';

const { Readable } = stream;

type MinerBaseOptions = {
  id?: string;
  dag?: DAG;
};

type MinerOptions = MinerBaseOptions & SurferOptions;

// Avoiding hard-coded message strings is out of scope of this exercise,
// Enum is not necessarily a good solution
class MinerBase {
  id: string;

  dag: DAG;

  private debug: Debugger;

  messageHandlers: Map<string, MessageHandler> = new Map();

  constructor({ id, dag }: MinerOptions) {
    this.id = id || uuid();
    this.dag = dag || new DAG({});

    this.messageHandlers.set('NEW_BLOCK', (message) => this.handleNewBlock(message));

    this.debug = Debug(`dag:miner:${this.id}`);
  }

  randomParentsMine() {
    const parentsCount = getRandomInt(1, this.dag.tips.length);
    const parents = this.dag.tips
      .sort(() => Math.random() - Math.random())
      .slice(0, parentsCount);

    const block = new Block({ parents });
    this.debug('adding new block %s with random parents %O', block.id, parents);

    this.dag.addBlock(block);
    return this.broadcastBlock(block);
  }

  // This should have a retry mechanism, but is out of scope
  broadcastBlock(block: Block): NodeJS.ReadableStream {
    this.debug('broadcasting block %s', block.id);
    const broadcastPromises = this.messageAllPeers(
      'NEW_BLOCK',
      JSON.stringify(block.serialize())
    );

    const readableStream = new Readable({
      objectMode: true,
      read() {},
    });

    readableStream.push(broadcastPromises.length);
    Promise.all(broadcastPromises).then(() => readableStream.push(null));

    return readableStream;
  }

  // Called when someone sends a new Block
  handleNewBlock(message: InternetReqRes): InternetReqRes {
    if (message.type !== 'NEW_BLOCK') {
      throw new Error('Wrong message type for NEW_BLOCK');
    }

    let parsedBlock;
    let block;
    try {
      parsedBlock = JSON.parse(message.content);
      block = Block.deserialize(parsedBlock);
    } catch (e) {
      return genErrorMessage(this.ip, message.from, 'Parse error');
    }

    this.debug('handling NEW_BLOCK %s from %s', block.id, message.from);
    const isAdded = this.dag.addBlock(block);

    if (isAdded) {
      this.debug('added block %s from %s', block.id, message.from);
    } else {
      this.debug('Error adding block %s from %s', block.id, message.from);
    }

    return {
      type: 'IS_BLOCK_ADDED',
      content: JSON.stringify({
        blockId: block.id,
        isAdded,
      }),
      from: this.ip,
      to: message.from,
    };
  }
}

// /////////////////////////////// //
//  Composition over inheritance  //
// ///////////////////////////// //

// declaration merging for the code in this file
interface MinerBase extends Surfer {}

// declaration merging for users of this code
const Miner = Surfer(MinerBase);
interface Miner extends MinerBase {}

export default Miner;
