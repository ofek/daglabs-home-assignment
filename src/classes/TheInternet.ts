import { v4 as uuid } from 'uuid';
import Debug from 'debug';

import Surfer from '~/mixins/Surfer';
import getRandomInt from '~/utils/getRandomInt';

// Independent of instance
const debug = Debug(`dag:the_internet`);

type TheInternetOptions = {
  propagationDelay: number;
};

export type IP = string;
export type InternetReqRes = {
  type: string;
  content: string;
  from: IP;
  to: IP;
};

// TheInternet™® is a place where Miners hang out and talk to each other
export default class TheInternet {
  surfers: Map<IP, Surfer> = new Map();

  propagationDelay: number;

  constructor({ propagationDelay }: TheInternetOptions) {
    this.propagationDelay = propagationDelay;
  }

  connect(surfer: Surfer): IP {
    const ip = uuid();
    this.surfers.set(ip, surfer);
    return ip;
  }

  sendMessage(message: InternetReqRes): Promise<InternetReqRes> {
    debug('sending request %O', message);
    const toSurfer = this.surfers.get(message.to);

    if (!toSurfer) {
      debug('destination disconnected for request %O', message);
      return Promise.reject(new Error('Destination disconnected'));
    }

    // TheInternet™® always resolves
    return new Promise((resolve) => {
      setTimeout(() => {
        const response = toSurfer.onMessage(message);
        resolve(response);
        debug('resolved request %O with response %O', response);
      }, this.getFactoredDelay());
    });
  }

  getFactoredDelay() {
    const congestionFactor = getRandomInt(-10, 11) / 100;
    const delay = this.propagationDelay;

    return delay + congestionFactor * delay;
  }
}
