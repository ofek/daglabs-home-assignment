import { v4 as uuid } from 'uuid';
import Debug, { Debugger } from 'debug';

export type SerializedBlock = {
  id: string;
  createdAt: Date;
  parents: SerializedBlock[];
};

type BlockOptions = {
  parents?: Block[];
  id?: string;
  createdAt?: Date;
};

export default class Block {
  id: string;

  createdAt: Date;

  parents: Block[];

  private debug: Debugger;

  constructor({ parents, id, createdAt }: BlockOptions) {
    this.parents = parents || [];
    this.id = id || uuid();
    this.createdAt = createdAt || new Date();

    this.debug = Debug(`dag:block:${this.id}`);
  }

  addParent(parent: Block) {
    this.parents.push(parent);
    this.debug('added parent %O', parent);
  }

  serialize(): SerializedBlock {
    return {
      id: this.id,
      createdAt: this.createdAt,
      parents: this.parents.map((p) => p.serialize()),
    };
  }

  static deserialize(serializedBlock: SerializedBlock, isNewId?: boolean) {
    try {
      const {
        parents: serializedParents,
        id,
        createdAt: serializedCreatedAt,
      } = serializedBlock;

      const createdAt = new Date(serializedCreatedAt);
      const parents: Block[] = serializedParents.map((p: SerializedBlock) =>
        Block.deserialize(p)
      );

      return new Block({ parents, id: isNewId ? undefined : id, createdAt });
    } catch (e) {
      throw new Error(e);
    }
  }
}
