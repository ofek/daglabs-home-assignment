module.exports = {
  root: true,
  env: {
    node: true,
  },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: './tsconfig.json',
    ecmaVersion: 2020,
  },
  plugins: ['@typescript-eslint'],
  extends: ['airbnb-typescript/base', 'prettier', 'prettier/@typescript-eslint'],
  rules: {
    // https://github.com/iamturns/eslint-config-airbnb-typescript/issues/95
    '@typescript-eslint/camelcase': 'off',
    // https://github.com/airbnb/javascript/issues/1271#issuecomment-514578455
    'no-restricted-syntax': [
      'error',
      {
        selector: 'ForInStatement',
        message:
          'for..in loops iterate over the entire prototype chain, which is virtually never what you want. Use Object.{keys,values,entries}, and iterate over the resulting array.',
      },
      {
        selector: 'LabeledStatement',
        message:
          'Labels are a form of GOTO; using them makes code confusing and hard to maintain and understand.',
      },
      {
        selector: 'WithStatement',
        message:
          '`with` is disallowed in strict mode because it makes code impossible to predict and optimize.',
      },
    ],
    // buggy
    'import/extensions': 'off',
  },
};
